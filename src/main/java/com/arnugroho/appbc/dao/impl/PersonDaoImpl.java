package com.arnugroho.appbc.dao.impl;

import java.util.Collection;

import com.arnugroho.appbc.dao.PersonDao;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arnugroho.appbc.model.Person;


@Repository
public class PersonDaoImpl implements PersonDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Person> listPerson() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = " from person";
		Query query = session.createQuery(hql);
		return query.list();

	}

	@Override
	public Person getPerson(int idPerson) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from person where id_person = :idPerson";
		Query query = session.createQuery(hql);
		query.setInteger("idPerson", idPerson);
		if (query.list().size() > 0) {
			return (Person) query.list().get(0);
		} else {
			return null;
		}
	}

	@Override
	public void insertPerson(Person person) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(person);
	}

	@Override
	public void updatePerson(Person person) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.update(person);

	}

	@Override
	public void deletePerson(Person person) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.delete(person);

	}

}
