package com.arnugroho.appbc.dao.impl;

import java.util.Collection;

import com.arnugroho.appbc.dao.GenderDao;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arnugroho.appbc.model.Gender;


@Repository
public class GenderDaoImpl implements GenderDao {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Gender> listGender() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from gender";
		Query query = session.createQuery(hql);
		return query.list();

	}

	@Override
	public void insertGender(Gender gender) throws Exception {
		Session session = sessionFactory.getCurrentSession();

		session.save(gender);

	}

	@Override
	public void updateGender(Gender gender) throws Exception {
		Session session = sessionFactory.getCurrentSession();

		session.update(gender);

	}

	@Override
	public void deleteGender(Gender gender) throws Exception {
		Session session = sessionFactory.getCurrentSession();

		session.delete(gender);

	}
	

}
