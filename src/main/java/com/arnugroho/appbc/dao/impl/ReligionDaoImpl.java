package com.arnugroho.appbc.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arnugroho.appbc.dao.ReligionDao;
import com.arnugroho.appbc.model.Religion;


@Repository
public class ReligionDaoImpl implements ReligionDao{
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Religion> listReligion() throws Exception {
		Session session = sessionFactory.getCurrentSession();

		String hql = " from religion order by id_religion asc";
		Query query = session.createQuery(hql);
		return query.list();
		
	}

	@Override
	public void insertReligion(Religion religion) throws Exception {
		Session session = sessionFactory.getCurrentSession();

		session.save(religion);

	}

	@Override
	public void updateReligion(Religion religion) throws Exception {
		Session session = sessionFactory.getCurrentSession();

		session.update(religion);
		
	}

	@Override
	public void deleteReligion(Religion religion) throws Exception {
		Session session = sessionFactory.getCurrentSession();

		session.delete(religion);
		
	}
	
}
