package com.arnugroho.appbc.dao;

import java.util.Collection;

import com.arnugroho.appbc.model.Person;

public interface PersonDao {
	public Collection<Person> listPerson () throws Exception;
	public Person getPerson (int idPerson) throws Exception;
	public void insertPerson (Person person) throws Exception;
	public void updatePerson (Person person) throws Exception;
	public void deletePerson (Person person) throws Exception;

}
