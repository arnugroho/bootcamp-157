package com.arnugroho.appbc.dao;

import java.util.Collection;

import com.arnugroho.appbc.model.Gender;


public interface GenderDao {
	public Collection<Gender> listGender () throws Exception;
	public void insertGender (Gender gender) throws Exception;
	public void updateGender (Gender gender) throws Exception;
	public void deleteGender (Gender gender) throws Exception;

}
