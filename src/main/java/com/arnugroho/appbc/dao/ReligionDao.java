package com.arnugroho.appbc.dao;

import java.util.Collection;

import com.arnugroho.appbc.model.Religion;


public interface ReligionDao {
	public Collection<Religion> listReligion () throws Exception;
	public void insertReligion (Religion religion) throws Exception;
	public void updateReligion (Religion religion) throws Exception;
	public void deleteReligion (Religion religion) throws Exception;

}
