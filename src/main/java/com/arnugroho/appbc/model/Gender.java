package com.arnugroho.appbc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "gender")
public class Gender {
	private int idGender;
	private String genderName;
	
	@Id
	@Column(name = "id_gender", length = 11, nullable = false)
	public int getIdGender() {
		return idGender;
	}
	public void setIdGender(int idGender) {
		this.idGender = idGender;
	}
	
	@Column(name = "gender_name", length = 45)
	public String getGenderName() {
		return genderName;
	}
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}
	
	


}
