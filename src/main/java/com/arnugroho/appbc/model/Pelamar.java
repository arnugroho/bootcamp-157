package com.arnugroho.appbc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "pelamar")
public class Pelamar {
	private String idPelamar;
	private String firstNamePelamar;
	private String lastNamePelamar;
	private Date birthDatePelamar;
	private int genderPelamar;
	private int religionPelamar;
	private String alamat;
	private Gender genderDesc;
	private Religion religionDesc;
	private String asalSekolah;
	
	@Id
	@Column(name = "id_pelamar", length = 11, nullable = false)
	public String getIdPelamar() {
		return idPelamar;
	}
	public void setIdPelamar(String idPelamar) {
		this.idPelamar = idPelamar;
	}
	
	@Column(name = "first_name_pelamar", length = 45)
	public String getFirstNamePelamar() {
		return firstNamePelamar;
	}
	public void setFirstNamePelamar(String firstNamePelamar) {
		this.firstNamePelamar = firstNamePelamar;
	}
	
	@Column(name = "last_name_pelamar", length = 45)
	public String getLastNamePelamar() {
		return lastNamePelamar;
	}
	public void setLastNamePelamar(String lastNamePelamar) {
		this.lastNamePelamar = lastNamePelamar;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "birthdate")
	public Date getBirthDatePelamar() {
		return birthDatePelamar;
	}
	public void setBirthDatePelamar(Date birthDatePelamar) {
		this.birthDatePelamar = birthDatePelamar;
	}
	
	@Column(name = "gender", length = 11)
	public int getGenderPelamar() {
		return genderPelamar;
	}
	public void setGenderPelamar(int genderPelamar) {
		this.genderPelamar = genderPelamar;
	}
	
	@Column(name = "religion", length = 11)
	public int getReligionPelamar() {
		return religionPelamar;
	}
	public void setReligionPelamar(int religionPelamar) {
		this.religionPelamar = religionPelamar;
	}
	
	@Column(name = "address", length = 45)
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	@ManyToOne
	@JoinColumn(name = "gender", updatable = false, insertable = false)
	public Gender getGenderDesc() {
		return genderDesc;
	}
	public void setGenderDesc(Gender genderDesc) {
		this.genderDesc = genderDesc;
	}
	
	@ManyToOne
	@JoinColumn(name = "religion", updatable = false, insertable = false)
	public Religion getReligionDesc() {
		return religionDesc;
	}
	public void setReligionDesc(Religion religionDesc) {
		this.religionDesc = religionDesc;
	}
	
	@Column(name = "sekolah", length = 45)
	public String getAsalSekolah() {
		return asalSekolah;
	}
	public void setAsalSekolah(String asalSekolah) {
		this.asalSekolah = asalSekolah;
	}
	
	
}
