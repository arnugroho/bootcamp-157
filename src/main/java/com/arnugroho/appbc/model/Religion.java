package com.arnugroho.appbc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "religion")
public class Religion {
	private int idReligion;
	private String religionName;
	
	@Id
	@Column(name = "id_religion", length = 11, nullable = false)
	public int getIdReligion() {
		return idReligion;
	}
	public void setIdReligion(int idReligion) {
		this.idReligion = idReligion;
	}
	
	@Column(name = "religion_name", length = 45)
	public String getReligionName() {
		return religionName;
	}
	public void setReligionName(String religionName) {
		this.religionName = religionName;
	}


}
