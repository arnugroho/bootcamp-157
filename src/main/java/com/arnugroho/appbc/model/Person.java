package com.arnugroho.appbc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "person")
public class Person {
	private int idPerson;
	private String firstName;
	private String lastName;
	private Date birthdate;
	private int gender;
	private int religion;
	private String address;
	private Gender genderDesc;
	private Religion religionDesc;
	
	@Id
	@Column(name = "id_person", length = 11, nullable = false)
	public int getIdPerson() {
		return idPerson;
	}
	public void setIdPerson(int idPerson) {
		this.idPerson = idPerson;
	}
	
	@Column(name = "first_name", length = 45)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name = "last_name", length = 45)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "birthdate")
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	@Column(name = "gender", length = 11)
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	
	@Column(name = "religion", length = 11)
	public int getReligion() {
		return religion;
	}
	public void setReligion(int religion) {
		this.religion = religion;
	}
	
	@Column(name = "address", length = 45)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@ManyToOne
	@JoinColumn(name = "gender", updatable = false, insertable = false)
	public Gender getGenderDesc() {
		return genderDesc;
	}
	public void setGenderDesc(Gender genderDesc) {
		this.genderDesc = genderDesc;
	}
	
	@ManyToOne
	@JoinColumn(name = "religion", updatable = false, insertable = false)
	public Religion getReligionDesc() {
		return religionDesc;
	}
	public void setReligionDesc(Religion religionDesc) {
		this.religionDesc = religionDesc;
	}
	

	
	


}
