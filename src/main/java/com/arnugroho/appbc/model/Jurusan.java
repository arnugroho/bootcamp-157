package com.arnugroho.appbc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "jurusan")
public class Jurusan {
	private int idJurusan;
	private String jurusanSekolahDesc;
	
	@Id
	@Column(name = "id_jurusan", length = 11, nullable = false)
	public int getIdJurusan() {
		return idJurusan;
	}
	public void setIdJurusan(int idJurusan) {
		this.idJurusan = idJurusan;
	}
	
	@Column(name = "nama_jurusan", length = 45)
	public String getJurusanSekolahDesc() {
		return jurusanSekolahDesc;
	}
	public void setJurusanSekolahDesc(String jurusanSekolahDesc) {
		this.jurusanSekolahDesc = jurusanSekolahDesc;
	}
}
