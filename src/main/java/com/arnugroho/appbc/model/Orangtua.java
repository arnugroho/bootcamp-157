package com.arnugroho.appbc.model;

import java.util.Date;

public class Orangtua {

	private int idOrangtua;
	private String firstName;
	private String lastName;
	private Date birthDate;
	private String address;
	private int gender;

	public int getIdOrangtua() {
		return idOrangtua;
	}

	public void setIdOrangtua(int idOrangtua) {
		this.idOrangtua = idOrangtua;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}
