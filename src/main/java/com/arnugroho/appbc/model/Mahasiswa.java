package com.arnugroho.appbc.model;

public class Mahasiswa {
	
	private int idMahasiswa;
	private String nama;
	private String alamat;
	private String tanggalLahir;
	private String gender;
	private String agama;
	
	public int getIdMahasiswa() {
		return idMahasiswa;
	}
	public void setIdMahasiswa(int idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAgama() {
		return agama;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	
	
	

}
