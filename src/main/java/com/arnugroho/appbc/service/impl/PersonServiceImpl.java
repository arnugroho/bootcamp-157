package com.arnugroho.appbc.service.impl;

import java.util.Collection;

import com.arnugroho.appbc.dao.PersonDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arnugroho.appbc.model.Person;
import com.arnugroho.appbc.service.PersonService;


@Service
@Transactional
public class PersonServiceImpl implements PersonService{
	
	@Autowired
	private PersonDao personDao ;
	
	@Override
	public Collection<Person> listPerson() throws Exception {
		// TODO Auto-generated method stub
		return personDao.listPerson();
	}
	
	@Override
	public Person getPerson(int idPerson) throws Exception {
		// TODO Auto-generated method stub
		return personDao.getPerson(idPerson);
	}
	
	@Override
	public void insertPerson(Person person) throws Exception {
		// TODO Auto-generated method stub
		personDao.insertPerson(person);
		
	}
	
	@Override
	public void updatePerson(Person person) throws Exception {
		// TODO Auto-generated method stub
		personDao.updatePerson(person);
		
	}
	
	@Override
	public void deletePerson(Person person) throws Exception {
		// TODO Auto-generated method stub
		personDao.deletePerson(person);
		
	}

}
