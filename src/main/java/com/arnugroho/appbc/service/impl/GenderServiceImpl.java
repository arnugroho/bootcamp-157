package com.arnugroho.appbc.service.impl;

import java.util.Collection;

import com.arnugroho.appbc.dao.GenderDao;
import com.arnugroho.appbc.service.GenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arnugroho.appbc.model.Gender;


@Service
@Transactional
public class GenderServiceImpl implements GenderService {
	
	@Autowired
	private GenderDao genderDao ;

	@Override
	public Collection<Gender> listGender() throws Exception {
		// TODO Auto-generated method stub
		return genderDao.listGender();
	}

	@Override
	public void insertGender(Gender gender) throws Exception {
		// TODO Auto-generated method stub
		genderDao.insertGender(gender);
	}

	@Override
	public void updateGender(Gender gender) throws Exception {
		// TODO Auto-generated method stub
		genderDao.updateGender(gender);
	}

	@Override
	public void deleteGender(Gender gender) throws Exception {
		// TODO Auto-generated method stub
		genderDao.deleteGender(gender);
		
	}

}
