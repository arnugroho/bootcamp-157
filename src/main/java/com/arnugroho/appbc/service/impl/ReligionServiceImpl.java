package com.arnugroho.appbc.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arnugroho.appbc.dao.ReligionDao;
import com.arnugroho.appbc.model.Religion;
import com.arnugroho.appbc.service.ReligionService;


@Service
@Transactional
public class ReligionServiceImpl implements ReligionService{
	
	@Autowired
	private ReligionDao religionDao ;

	@Override
	public Collection<Religion> listReligion() throws Exception {
		// TODO Auto-generated method stub
		return religionDao.listReligion();
	}

	@Override
	public void insertReligion(Religion religion) throws Exception {
		// TODO Auto-generated method stub
		religionDao.insertReligion(religion);
		
	}

	@Override
	public void updateReligion(Religion religion) throws Exception {
		// TODO Auto-generated method stub
		religionDao.updateReligion(religion);
		
	}

	@Override
	public void deleteReligion(Religion religion) throws Exception {
		// TODO Auto-generated method stub
		religionDao.deleteReligion(religion);
		
	}

}
