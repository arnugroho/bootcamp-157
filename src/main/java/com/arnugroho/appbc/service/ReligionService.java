package com.arnugroho.appbc.service;

import java.util.Collection;

import com.arnugroho.appbc.model.Religion;


public interface ReligionService {
	public Collection<Religion> listReligion () throws Exception;
	public void insertReligion (Religion religion) throws Exception;
	public void updateReligion (Religion religion) throws Exception;
	public void deleteReligion (Religion religion) throws Exception;

}
