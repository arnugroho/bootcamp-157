package com.arnugroho.appbc.service;

import java.util.Collection;

import com.arnugroho.appbc.model.Gender;


public interface GenderService {
	public Collection<Gender> listGender () throws Exception;
	public void insertGender (Gender gender) throws Exception;
	public void updateGender (Gender gender) throws Exception;
	public void deleteGender (Gender gender) throws Exception;

}
