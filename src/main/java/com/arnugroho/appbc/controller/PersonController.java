package com.arnugroho.appbc.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import com.arnugroho.appbc.model.Person;
import com.arnugroho.appbc.model.Religion;
import com.arnugroho.appbc.service.PersonService;
import com.arnugroho.appbc.service.ReligionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class PersonController {

    @Autowired
    PersonService personService;
    @Autowired
    ReligionService religionService;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = { "/", "/home.html", "/person-update.html" }, method = RequestMethod.GET)
    public String home(Model model, HttpServletRequest request) {

        request.setAttribute("action", "insert");
        if (request.getParameter("idPerson") != null) {
            try {
                Person person = new Person();
                person = personService.getPerson(Integer.parseInt(request.getParameter("idPerson")));
                request.setAttribute("idPerson", Integer.parseInt(request.getParameter("idPerson")));
                request.setAttribute("firstName", person.getFirstName());
                request.setAttribute("lastName", person.getLastName());
                request.setAttribute("birthdate", person.getBirthdate());
                request.setAttribute("gender", person.getGender());
                request.setAttribute("religion", person.getReligion());
                request.setAttribute("address", person.getAddress());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                request.setAttribute("errorMessage", e.getMessage());

            }

            request.setAttribute("action", request.getParameter("action"));
        } else {
            request.setAttribute("action", "insert");
        }

        defaultList(request);

        return "person-list";
    }

    @RequestMapping(value = { "/person-update.html", "/person-insert" }, method = RequestMethod.POST)
    public String insert(Model model, HttpServletRequest request) {

        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("insert")) {
            try {
                Person person = new Person();
                person.setFirstName(request.getParameter("firstName"));
                person.setLastName(request.getParameter("lastName"));
                person.setBirthdate(sdf.parse(request.getParameter("birthdate")));
                person.setGender(Integer.parseInt(request.getParameter("gender")));
                person.setReligion(Integer.parseInt(request.getParameter("religion")));
                person.setAddress(request.getParameter("address"));
                personService.insertPerson(person);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                request.setAttribute("errorMessage", e.getMessage());

            }
            request.setAttribute("action", "insert");
        } else if (action.equalsIgnoreCase("edit")) {
            try {
                Person person = new Person();
                person.setIdPerson(Integer.parseInt(request.getParameter("idPerson")));
                person.setFirstName(request.getParameter("firstName"));
                person.setLastName(request.getParameter("lastName"));
                person.setBirthdate(sdf.parse(request.getParameter("birthdate")));
                person.setGender(Integer.parseInt(request.getParameter("gender")));
                person.setReligion(Integer.parseInt(request.getParameter("religion")));
                person.setAddress(request.getParameter("address"));
                personService.updatePerson(person);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                request.setAttribute("errorMessage", e.getMessage());

            }
            request.setAttribute("action", "insert");
        }
        defaultList(request);
        return "person-list";
    }

    @RequestMapping(value = "/person-delete.html", method = RequestMethod.POST)
    public String delete(Model model, HttpServletRequest request) {

        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("delete")) {
            int idPerson = Integer.parseInt(request.getParameter("idPerson"));
            Person person = new Person();
            person.setIdPerson(idPerson);

            try {
                personService.deletePerson(person);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                request.setAttribute("errorMessage", e.getMessage());

            }
            request.setAttribute("action", "insert");
        }

        defaultList(request);
        return "person-list";
    }

    public void defaultList(HttpServletRequest request) {

        Collection<Person> personCollection = new ArrayList<Person>();
        Collection<Religion> religionCollection = new ArrayList<Religion>();
        try {
            personCollection = personService.listPerson();
            religionCollection = religionService.listReligion();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            request.setAttribute("errorMessage", e.getMessage());

        }
        request.setAttribute("personCollection", personCollection);
        request.setAttribute("religionCollection", religionCollection);

    }

    @RequestMapping(value = "/halaman-utama")
    public String halamanUtama(Model model, HttpServletRequest request) {
        try {
            Collection<Religion> religion = religionService.listReligion();
            model.addAttribute("listReligion",religion);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "person-list";
    }

}
