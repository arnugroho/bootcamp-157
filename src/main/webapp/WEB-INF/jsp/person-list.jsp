<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="contextPath" value="${pageContext.request.contextPath }" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<c:set var="idGender" scope="session" value="${gender }" />
<c:if test="${gender == 2}">
	<c:set var="checked" scope="session" value="checked" />
</c:if>

<!-- stylesheet -->
<link href="${contextPath }/ASSETS/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<style type="text/css">
.table thead tr th, .column_center {
	text-align: center;
}
</style>

<!-- javascript / jquery -->
<script type="text/javascript"
	src="${contextPath }/ASSETS/js/jquery-1.11.1.js"></script>
<script type="text/javascript"
	src="${contextPath }/ASSETS/js/bootstrap.min.js"></script>
<body>
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${contextPath }">Quiz 1 Java</a>
		</div>
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false">Personal
						Detail <span class="caret"></span>
				</a>
					<ul class="dropdown-menu" role="menu">
						<li class="active"><a
							href="${contextPath }/religion-list.html">Religion</a></li>
						<li><a href="${contextPath }/gender-list.html">Gender</a></li>
					</ul></li>
			</ul>
			<p class="navbar-text navbar-right">Dynamic WEB Project Sample</p>
		</div>
	</div>
	</nav>


	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div id="res-ajax"></div>
						<c:if test="${not empty requestScope.errorMessage }">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<strong>Error!</strong> ${requestScope.errorMessage }.
							</div>
						</c:if>

						<form id="form-personal-detail" action="person-insert.html"
							method="post" autocomplete="off">
							<input type="hidden" name="action" value="${action }"> <input
								type="hidden" name="idPerson" value="${idPerson }">
							<fieldset>
								<legend>Personal Detail Form</legend>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>First Name <i class="text-danger">*</i></label> <input
												type="text" class="form-control" id="firstName"
												name="firstName" value="${firstName }" required="required"
												placeholder="Your First Name" />
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Last Name <i class="text-danger">*</i></label> <input
												type="text" class="form-control" id="lastName"
												name="lastName" value="${lastName }" required="required"
												placeholder="Your Last Name" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group">
													<label>Gender</label> <select class="form-control"
														id="gender" name="gender">
														<option value="">-- Choose --</option>
														<option value="1">Male</option>
														<option value="2">Female</option>
													</select>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group">
													<label>Religion</label> <select class="form-control"
														id="religion" name="religion">
														<option value="1">Islam</option>
														<option value="2">Kristen</option>
														<option value="3">Katolik</option>
														<option value="4">Hindu</option>
														<option value="5">Budha</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Date of Birth</label> <input type="date"
												class="form-control" id="birthdate" name="birthdate"
												value="${birthdate }" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label>Address</label>
											<textarea class="form-control" rows="3" id="address"
												name="address">${address }</textarea>
										</div>
									</div>
								</div>
								<hr />
								<div class="row">
									<div class="col-sm-12">
										<button id="btn-reset" type="button" class="btn btn-danger">
											<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;&nbsp;Reset
										</button>
										<button type="submit" class="btn btn-success pull-right">
											<span class="glyphicon glyphicon-floppy-saved"
												aria-hidden="true"></span>&nbsp;&nbsp;Submit
										</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">

				<table class="table table-bordered table-condensed table-hover"
					style="margin-top: 15px;">
					<thead>
						<tr>
							<th width="10%">ID</th>
							<th>First Name</th>
							<td>Last Name</td>
							<td>Birthdate</td>
							<td>Gender</td>
							<td>Religion</td>
							<td>Address</td>
							<td>JSP-GET</td>
							<td>JSP-POST</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="person" items="${personCollection }">
							<tr>
								<td class="column_center">${person.idPerson }</td>
								<td>${person.firstName }</td>
								<td>${person.lastName }</td>
								<td>${person.birthdate }</td>
								<td>${person.genderDesc.genderName }</td>
								<td>${person.religionDesc.religionName }</td>
								<td>${person.address }</td>
								<!-- jsp-get -->
								<td><a class="btn btn-warning"
									href="person-update.html?action=edit&idPerson=<c:out value="${person.idPerson}"/>">
										<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a></td>
								<!-- jsp-post -->
								<td>
									<form action="person-delete.html" method="post">
										<button class="btn btn-warning" type="submit">
											<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
											<input type="hidden" name="idPerson"
												value="<c:out value="${person.idPerson}" />"> <input
												type="hidden" name="action" value="delete">
										</button>
									</form>

								</td>
								<td class="column_center">
									<button class="btn btn-warning"
										onclick="dirrectToForm('${person.idPerson }')">
										<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
									</button>
								</td>
								<td class="column_center">
									<button class="btn btn-danger"
										onclick="doDelete('${person.idPerson }')">
										<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
									</button>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</div>
		</div>

	</div>
	<script type="text/javascript">
		$('#btn-reset').click(function() {
			console.log('${listReligion}')
			//alert(${listReligion})
			//window.location.replace("home.html");
		});

		$(document).ready(function() {
			var gender = '${gender }';
			$("#form-personal-detail :input[name='gender']").val(gender);
		});

		function dirrectToForm(param) {
			window.location.replace("person-update.html?action=edit&idPerson="
					+ param);
		}

		function doDelete(param) {
			$.ajax({
				type : "POST",
				url : "person-delete.html",
				async : true,
				data : {
					action : "delete",
					idPerson : param
				},
				success : function(response) {
					
					if (response.isSuccess == true) {
						location.reload(true);
					} else {
						$('#res-ajax').html("<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Error!</strong> " +response.errorMessage +"</div>");
					}

				}

			});
		}
	</script>
</body>
</html>