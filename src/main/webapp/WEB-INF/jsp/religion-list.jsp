<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div id="ReligionForm">
		<form action="religion-list.html" method="post">
			<table>
	
				<tr>
					<td>Id Religion</td>
					<td>:</td>
					<td><input type="text" name="idReligion" value="${idReligion }"></td>
				</tr>
				<tr>
					<td>Religion</td>
					<td>:</td>
					<td><input type="text" name="religionName" value="${religionName }"></td>
				</tr>
	
			</table>
			<input type="hidden" name="action" value="${action }">
			<input type="submit" value="${action }">
		</form>
	</div>
	<div id="ReligionList" >
		<table border="1">
			<thead>
				<tr>
					<td>Id Religion</td>
					<td>Religion</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${religionCollection}" var="religion">
					<tr>
						<td><c:out value="${religion.idReligion}" /></td>
						<td><c:out value="${religion.religionName}" /></td>
						<td><a
							href="religion-list.html?action=edit&idReligion=<c:out value="${religion.idReligion}"/>&religionName=<c:out value="${religion.religionName}" />">Edit</a></td>
						<td>
							<form action="religion-list.html" method="post">
								<input type="submit" name="action" value="delete">
								<input type="hidden" name="idReligion" value="<c:out value="${religion.idReligion}" />">
							</form>
						</td>
					</tr>
					
						
					
				</c:forEach>
			</tbody>
		</table>
	</div>


</body>
</html>