<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div id="mainBody">
		<table border="1">
			<thead>
				<tr>
					<td>Id Gender</td>
					<td>Gender</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${genderCollection}" var="gender">
					<tr>
						<td><c:out value="${gender.idGender}" /></td>
						<td><c:out value="${gender.genderName}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>